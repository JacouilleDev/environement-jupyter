# Environement Jupyter
## Déploiment
Simplement lancer le script run (`run.bat` pour Windows et `run.sh` pour OSX).

Le script va télécharger l'image la plus récente de l'Environement Jupyter, puis le lancer.

## Informations
Le script créer également un volume docker, ce dernier connecte un dossier de votre machine et le dossier de travail sur le container. Cela permet de persister les fichiers de travail sur la machine et de ne pas les perdres en cas de redémarrage ou suppression du container.

Ce dossier se trouvera à la racine de votre dossier personnel (`C:\Users\<votre username>\EnvironementJupyter` pour Windows et `/Users/<votre username>` pour OSX).

## Pour aller plus loin
Quelques commandes utiles pour Docker (les mêmes pour Windows et OSX)
#### Supprimer tous les containers, les volumes ainsi que les fichiers temporaires
> Cette commande ne supprimera pas votre travail présent dans le dossier `EnvironementJupyter`, seulement le volume qui le relie à un container. Il n'y as donc aucun risque avec cette commande. Il faudra seulement re télécharger entièrement le container avec le script `run.bat` ou `run.sh`.
```shell
docker system prune -a
```
