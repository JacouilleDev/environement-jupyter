@ECHO OFF

:::         __                  __            ____
:::        / /_  ______  __  __/ /____  _____/ __ \____ _____
:::   __  / / / / / __ \/ / / / __/ _ \/ ___/ /_/ / __ `/ __ \
:::  / /_/ / /_/ / /_/ / /_/ / /_/  __/ /  / ____/ /_/ / / / /
:::  \____/\__,_/ .___/\__, /\__/\___/_/  /_/    \__,_/_/ /_/
:::            /_/    /____/
for /f "delims=: tokens=*" %%A in ('findstr /b ::: "%~f0"') do @echo(%%A

ECHO == Stopping old Jupyter Environment ...

docker stop environement-jupyter

ECHO == Removing old Jupyter Environment ...

docker rm environement-jupyter

ECHO == Pulling new Jupyter Environment ...

docker pull registry.gitlab.com/jacouilledev/environement-jupyter:latest

ECHO == Starting new Jupyter Environment ...

docker run --name environement-jupyter -v "C:\Users\%username%\EnvironementJupyter:/home" -d -p 8888:8888 registry.gitlab.com/jacouilledev/environement-jupyter:latest

ECHO == Jupyter Environment Started !

exit