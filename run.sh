echo "        __                  __            ____"
echo "       / /_  ______  __  __/ /____  _____/ __ \____ _____"
echo "  __  / / / / / __ \/ / / / __/ _ \/ ___/ /_/ / __  / __ \\"
echo " / /_/ / /_/ / /_/ / /_/ / /_/  __/ /  / ____/ /_/ / / / /"
echo " \____/\__,_/ .___/\__, /\__/\___/_/  /_/    \__,_/_/ /_/"
echo "           /_/    /____/"

echo "== Stopping old Jupyter Environment ..."

docker stop environement-jupyter

echo "== Removing old Jupyter Environment ..."

docker rm environement-jupyter

echo "== Pulling new Jupyter Environment ..."

docker pull registry.gitlab.com/jacouilledev/environement-jupyter:latest

echo "== Starting new Jupyter Environment ..."

docker run --name environement-jupyter -v "/Users/$USER/EnvironementJupyter:/home" -d -p 8888:8888 registry.gitlab.com/jacouilledev/environement-jupyter:latest

echo "== Jupyter Environment Started !"