FROM python:3.10-bullseye

RUN apt-get update && apt-get upgrade -y && apt-get install -y && apt-get -y install apt-utils gcc libpq-dev libsndfile-dev

WORKDIR /home

ENV VIRTUAL_ENV=env/bin/activate
RUN python3 -m venv VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
CMD . env/bin/activate

RUN pip3 install notebook

RUN pip3 install ipython==8.4.0
RUN pip3 install ipywidgets==8.0.1
RUN pip3 install matplotlib==3.5.0
RUN pip3 install scikit_image==0.18.3
RUN pip3 install scikit_learn==1.1.2
RUN pip3 install SoundFile==0.10.3.post1
RUN pip3 install torch==1.12.1
RUN pip3 install torchvision==0.13.1

RUN pip3 install sndfile

EXPOSE 8888
CMD ["jupyter", "notebook", "--ip", "0.0.0.0", "--port", "8888", "--no-browser", "--allow-root"]